import pymysql
global base


def connect():
    base = pymysql.connect(
    host = "localhost",
    user = "root",
    passwd = "Idontlikethis_23",
    db = "test",
    charset = 'utf8',
    cursorclass = pymysql.cursors.DictCursor
    )
    return base

def create_client_list(base):
    with base.cursor() as cursor:
        sql = "CREATE TABLE `client_list` (uid varchar(100) NOT NULL, name varchar(100) NOT NULL)"
        cursor.execute(sql)

def create_logs(base):
    with base.cursor() as cursor:
        sql = "CREATE TABLE `logs` (id int NOT NULL AUTO_INCREMENT, uid varchar(100) NOT NULL, location varchar(100) NOT NULL, counter int NOT NULL, time varchar(100) NOT NULL, PRIMARY KEY(id))"
        cursor.execute(sql)

def create_unknown(base):
    with base.cursor() as cursor:
        sql = "CREATE TABLE `unknown` (id int NOT NULL AUTO_INCREMENT, uid varchar(100) NOT NULL, location varchar(100) NOT NULL, time varchar(100) NOT NULL, PRIMARY KEY(id))"
        cursor.execute(sql)


def main():
    test = connect()
    create_client_list(test)
    create_logs(test)
    create_unknown(test)
    test.close()

main()