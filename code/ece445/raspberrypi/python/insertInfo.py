'''Code used to read an NFC band and check if the user is registered in the database or not. 
If the user is not registered you will be notified and prompted if you want to add this user to
the database. If you add them you will be asked to enter additional information. If not added the code will
do nothing and the database will close. 

DONT FORGET TO ADD REFERENCES TO CODE THAT WAS MODIFIED INCLUDE THE COPYRIGHT CLAIM!!!!!!!

'''

import pymysql
import RPi.GPIO as GPIO
from pn532 import*
global base
#code used to open the data base to modify, delete, or add information
def connect_data():
    base = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db='test',
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )
    return base

def read_database_credit(base): 

    with base.cursor() as cursor:
            # Read data from database
        sql = "SELECT * FROM `client_list`"
        cursor.execute(sql)
            # Fetch all rows
        rows = cursor.fetchall()
            # Print results
        for row in rows:
            print(row)




def write_database_credit(base, userID):
    with base.cursor() as cursor:
        # Create a new record
        name = input("What is the users name?")
        sql = "INSERT INTO `client_list` (`uid`, `name`) VALUES (%s, %s)"
        cursor.execute(sql, (userID, name))

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    base.commit()

def delete_entry_credit(base, userID):
    with base.cursor() as cursor:
        sql = "DELETE FROM `client_list` WHERE `uid`=%s"
        cursor.execute(sql, (userID))
    base.commit()


def addZero(x):
    length = len(x)
    if length == 1:
        x = '0'+x
        return x
    else:
        return x


def modify_credit_table(base):
    try:
        #pn532 = PN532_SPI(debug=False, reset=20, cs=4)
        #pn532 = PN532_I2C(debug=False, reset=20, req=16)
        pn532 = PN532_UART(debug=False, reset=20)

        # Configure PN532 to communicate with MiFare cards
        pn532.SAM_configuration()

        print('Waiting for RFID/NFC card...')

        fuid = ''
        while True:
            # Check if a card is available to read
            uid = pn532.read_passive_target(timeout=0.5)
            print('.', end="")
            # Try again if no card is available.
            if uid is None:
                continue 
            temp = [hex(i) for i in uid]
            for b in temp:
                len_of_uid = len(b)
                if len_of_uid == 3:
                    hex_temp = "0"+b[2]
                
                if len_of_uid == 4:
                    hex_temp = b[2] + b[3]
                
                fuid += hex_temp

            with base.cursor() as cursor:
                sql = "SELECT `uid` FROM `client_list` WHERE `uid`=%s"
                cursor.execute(sql, (fuid))
                data = cursor.fetchall()
                if data:
                    print("User is already in the system")
                    answer = input("Do you wish to delete this user? ").lower()
                    if answer == "yes":
                        delete_entry_credit(base, fuid)
                    else:
                        continue
                else:
                    write_database_credit(base, fuid)
            break
            
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()

def read_tables(base):
    with base.cursor() as cursor:
        sql = "SHOW TABLES"
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            print(row)


def main():

    test = connect_data()
    try:
        tables = input("Do you wish to work with an indiviual table? ").lower()
        if tables == "yes":
            read_tables(test)
            i_table = input("From the tables above which one will you like to modify? ")
            if i_table == "client_list":
                answer = input("Do you wish to read or write? ").lower()
                while True:
                    if answer == "read":
                        read_database_credit(test)
                        break
                    elif answer == "write":
                        modify_credit_table(test)
                        break
                    else:
                        break
        else:
            read_tables(test)
    finally:
        test.close()
        print("exit was successful")