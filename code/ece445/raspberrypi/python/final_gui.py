from gui import *
global db_click, table_click, tree

def main():
    delete_all_logs()
    delete_all_unknown()

    options_table = get_options_table()
    options_database = ['test']

    # create a new tkinter window
    window = tk.Tk()

    # set the window title
    window.title("SQL Table Viewer")

    table_click = StringVar()
    table_click.set("Choose Table")

    db_click = StringVar()
    db_click.set("Choose DB")

    # create a label widget for the database name
    db_name_label = tk.Label(window, text="Database Name:")
    db_name_label.grid(row=0, column=0, padx=5, pady=5)

    # create an entry widget for the database name
    db_name_entry = tk.OptionMenu(window, db_click, *options_database)
    db_name_entry.grid(row=0, column=1, padx=5, pady=5)

    # create a label widget for the table name
    table_name_label = tk.Label(window, text="Table Name:")
    table_name_label.grid(row=1, column=0, padx=5, pady=5)

    # create an entry widget for the table name
    table_name_entry = tk.OptionMenu(window, table_click, *options_table)
    table_name_entry.grid(row=1, column=1, padx=5, pady=5)

    # # create a Treeview widget to display the table data
    tree = ttk.Treeview(window, columns=("column1", "column2", "column3", "column4", "column5"), show="headings")
    tree.grid(row=2, column=0, columnspan=2, padx=5, pady=5)

    # create a button widget to populate the table
    populate_button = tk.Button(window, text="Populate Table", command=populate_table())
    populate_button.grid(row=3, column=0, padx=5, pady=5)

    #create a button for options 
    options_button = tk.Button(window, text="table options", command=select)
    options_button.grid(row=6, column=6, padx=5, pady=5)

    window.mainloop()

main()