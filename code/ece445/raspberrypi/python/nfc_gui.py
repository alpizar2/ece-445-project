import RPi.GPIO as GPIO
from pn532 import*
import pymysql.cursors



def add_user_nfc():
    try:
        pn532 = PN532_UART(debug=False, reset=20)
        pn532.SAM_configuration()
        print("Waiting for RFID/NFC card")
        fhex = ""

        while True:
            uid = pn532.read_passive_target(timeout = 0.5)
            #if not card is read then repeat
            if uid is None:
                continue
            temp = [hex(i) for i in uid]
            for b in temp:
                len_of_uid = len(b)
                if len_of_uid == 3:
                    hex_temp = "0"+b[2]
                
                if len_of_uid == 4:
                    hex_temp = b[2] + b[3]
                
                fhex += hex_temp
            break

    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
    
    return fhex

def delete_all_logs():
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db="test",
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()
    sql = "DELETE FROM `logs`"
    cursor.execute(sql)
    cnx.commit()
    cursor.close()
    cnx.close()

def delete_all_unknown():
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db="test",
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()
    sql = "DELETE FROM `unknown`"
    cursor.execute(sql)
    cnx.commit()
    cursor.close()
    cnx.close()

def get_options_table():
    cnx = pymysql.connect(
    host='localhost',
    user='root',
    password='Idontlikethis_23',
    db="test",
    charset='utf8',
    cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()
    sql = "SHOW TABLES"
    cursor.execute(sql)
    result = cursor.fetchall()

    temp_list = []
    for i in range(len(result)):
        # print(i)
        temp_list.append(result[i]["Tables_in_test"])
    # print(temp_list)
    cursor.close()
    cnx.close()

    return temp_list

def get_options_db():
    cnx = pymysql.connect(
    host='localhost',
    user='root',
    password='Idontlikethis_23',
    db="test",
    charset='utf8',
    cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()
    sql = "SHOW DATABASES"
    cursor.execute(sql)
    result = cursor.fetchall()

    temp_list = []
    for i in range(len(result)):
        # print(i)
        temp_list.append(result[i]["Database"])
    # print(temp_list)
    cursor.close()
    cnx.close()

    return temp_list

def check_db(uid_):
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db="test",
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()
    query = "SELECT CASE WHEN EXISTS (SELECT 1 FROM `client_list` WHERE uid = %s) THEN 'TRUE' ELSE 'FALSE' END"
    cursor.execute(query, (uid_))
    rows = cursor.fetchall()
    temp = "CASE WHEN EXISTS (SELECT 1 FROM `client_list` WHERE uid = '{}') THEN 'TRUE' ELSE 'FALSE' END".format(uid_)

    return rows[0][temp]

# def get_logs_liable():
#     cnx = pymysql.connect(
#         host = 'localhost',
#         user='root',
#         password = 'Idontlikethis_23',
#         db='test',
#         charset='utf8',
#         cursorclass=pymysql.cursors.DictCursor
#     )

#     cursor = cnx.cursor()

#     query = "SELECT `uid` FROM `logs` WHERE counter > 1"
#     #query = "SELECT * FROM `client_list`"
#     cursor.execute(query)

#     result = cursor.fetchall()
#     complete = {}
#     for row in result:
#         temp = row['uid']
#         if temp not in complete.keys():
#             complete[temp] = {}

#     for uid in complete.keys():
#         print(uid)
#         new_query = "SELECT `name` FROM `client_list` WHERE uid=%s"
#         cursor.execute(new_query, (uid))
#         result = cursor.fetchall()
#         for row in result:
#             temp = row['name']
#             if temp not in complete[uid]:
#                 complete[uid][temp] = {}


#     time_list = []

#     for uid in complete.keys():
#         new_query = "SELECT `time` FROM `logs` WHERE uid=%s"
#         cursor.execute(new_query, (uid))
#         result = cursor.fetchall()
#         for name in complete[uid].keys():
#             for row in result:
#                 print(row)
#                 time_list.append(row['time'])
#             complete[uid][name] = time_list


#     cursor.close()
#     cnx.close()

#     print(complete)
#     uid_list = []
#     name_list = []
#     timer_list = []
#     for uid in complete.keys():
#         print(uid)
#         for name in complete[uid].keys():
#             print(name)
#             temp = len(complete[uid][name])
#             print(temp)
#             for time in complete[uid][name]:
#                 print(time)
#                 timer_list.append(time)
#                 uid_list.append(uid)
#                 name_list.append(name)

#     count = len(uid_list)
   
#     return uid_list, name_list, timer_list, count



