import tkinter as tk
from tkinter import ttk
from tkinter import *
from nfc_gui import *
global db_click, table_click, tree



# define a function to populate the Treeview widget with data
def populate_table():
    # get the database name and table name from the entry widgets
    db_name = db_click.get()
    # table_name = table_name_entry.get()
    table_name = table_click.get()
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )
    # create a cursor object
    cursor = cnx.cursor()

    # execute a SELECT statement to retrieve the data from the table
    query = f"SELECT * FROM {table_name}"
    cursor.execute(query)

    # get the column names from the cursor description
    column_names = [i[0] for i in cursor.description]

    # set the columns in the Treeview widget
    tree["columns"] = column_names
    for col in column_names:
        tree.heading(col, text=col)

    # clear all items from the treeview widget
    tree.delete(*tree.get_children())
    # populate the Treeview widget with the data
    for row in cursor.fetchall():
        tree.insert("", "end", values=list(row.values()))

    # close the cursor and connection
    cursor.close()
    cnx.close()

def select():
    option = table_click.get()

    if option == "unknown":
        delete_table_button = tk.Button(window, text="Delete Table", command=show_delete_table_form)
        delete_table_button.grid(row=6, column=2, padx=5, pady=5)
    
    if option == "logs":
        delete_table_button = tk.Button(window, text="Delete Table", command=show_delete_table_form)
        delete_table_button.grid(row=6, column=2, padx=5, pady=5)
    
    if option == "client_list":
        delete_table_button = tk.Button(window, text="Delete Table", command=show_delete_table_form)
        delete_table_button.grid(row=6, column=2, padx=5, pady=5)

        delete_manual_data_button = tk.Button(window, text="Delete Manual", command=show_delete_manual_form)
        delete_manual_data_button.grid(row=6, column=3, padx=5, pady=5)


        delete_data_button = tk.Button(window, text="Delete Data", command=show_delete_data_form)
        delete_data_button.grid(row=6, column=4, padx=5, pady=5)


        add_data_button = tk.Button(window, text="Add Data", command=show_add_data_form)
        add_data_button.grid(row=6, column=5, padx=5, pady=5)

# define a function to show the form for adding data
def show_add_data_form():
    # create a new tkinter window for the form
    form_window = tk.Toplevel(window)

    # set the window title
    form_window.title("Add Data")

    # create a label widget for the first column
    # column1_label = tk.Label(form_window, text="uid:")
    # column1_label.grid(row=0, column=0, padx=5, pady=5)

    # create an entry widget for the first column
    # column1_entry = tk.Entry(form_window)
    # column1_entry.grid(row=0, column=1, padx=5, pady=5)

    # create a label widget for the second column
    column2_label = tk.Label(form_window, text="name:")
    column2_label.grid(row=1, column=0, padx=5, pady=5)

    # create an entry widget for the second column
    column2_entry = tk.Entry(form_window)
    column2_entry.grid(row=1, column=1, padx=5, pady=5)

    # # create a label widget for the third column
    # column3_label = tk.Label(form_window, text="Column 3:")
    # column3_label.grid(row=2, column=0, padx=5, pady=5)

    # # create an entry widget for the third column
    # column3_entry = tk.Entry(form_window)
    # column3_entry.grid(row=2, column=1, padx=5, pady=5)

    # define a function to insert the data into the database
def insert_data(db_click, table_click):
        # get the database name and table name from the entry widgets
    db_name = db_click.get()
    table_name = table_click.get()

        # get the data from the entry widgets
        #column1 = column1_entry.get()
    column1 = add_user_nfc()
    print(column1)
    column2 = column2_entry.get()
        #column3 = column3_entry.get()

        # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

        # create a cursor object
    cursor = cnx.cursor()

        # execute an INSERT statement to insert the data into the table
    query = f"INSERT INTO {table_name} (uid, name) VALUES (%s, %s)" #, column3 , %s
    values = (column1, column2) #, column3)
    cursor.execute(query, values)

    # commit the transaction and close the cursor and connection
    cnx.commit()
    cursor.close()
    cnx.close()

    # close the form window
    form_window.destroy()

    #tk.update()
    # repopulate the table with the new data
    populate_table()

    # create a button widget to insert the data
    insert_button = tk.Button(form_window, text="Scan NFC", command=insert_data)
    insert_button.grid(row=3, column=0, columnspan=2, padx=5, pady=5)

# define a function to delete data from the database
def delete_data(db_click, table_click):
    # get the database name, table name, and column value from the entry widgets
    db_name = db_click.get()
    table_name = table_click.get()
    #column_value = column_value_entry.get()
    #print(column_value)
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    # create a cursor object
    cursor = cnx.cursor()
    chip = add_user_nfc()
    # execute a DELETE statement to delete the data from the table
    query = "DELETE FROM `client_list` WHERE uid= %s"
    cursor.execute(query, (chip))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    cursor.close()
    cnx.close()

    # close the form window
    form_window.destroy()

    # repopulate the table with the updated data
    populate_table()

# define a function to show the delete data form
def show_delete_data_form():
    global form_window

    # create a new window for the form
    form_window = tk.Toplevel()
    form_window.title("Delete Client")

    # create labels and entry widgets for the form
    # db_name_label = tk.Label(form_window, text="Database Name:")
    # db_name_label.grid(row=0, column=0, padx=5, pady=5)
    # db_name_entry = tk.Entry(form_window)
    # db_name_entry.grid(row=0, column=1, padx=5, pady=5)

    # table_name_label = tk.Label(form_window, text="Table Name:")
    # table_name_label.grid(row=1, column=0, padx=5, pady=5)
    # table_name_entry = tk.Entry(form_window)
    # table_name_entry.grid(row=1, column=1, padx=5, pady=5)

    column_value_label = tk.Label(form_window, text="Scan NFC of user to delete.")
    column_value_label.grid(row=2, column=0, padx=5, pady=5)
    # column_value_entry = tk.Entry(form_window)
    # column_value_entry.grid(row=2, column=1, padx=5, pady=5)

    # create a button widget to delete the data
    delete_button = tk.Button(form_window, text="Delete Data", command=delete_data)
    delete_button.grid(row=3, column=0, columnspan=2, padx=5, pady=5)

def delete_manual(db_click):
    # get the database name, table name, and column value from the entry widgets
    db_name = db_click.get()
    # table_name = table_name_entry.get()
    column_value = column_value_entry.get()
    #print(column_value)
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    # create a cursor object
    cursor = cnx.cursor()
    # execute a DELETE statement to delete the data from the table
    query = f"DELETE FROM `client_list` WHERE uid=%s"
    cursor.execute(query, (column_value))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    cursor.close()
    cnx.close()

    # close the form window
    form_window.destroy()

    # repopulate the table with the updated data
    populate_table()

def show_delete_manual_form():
    global db_click,column_value_entry, form_window

    # create a new window for the form
    form_window = tk.Toplevel()
    form_window.title("Delete Client.")

    # create labels and entry widgets for the form
    # db_name_label = tk.Label(form_window, text="Database Name:")
    # db_name_label.grid(row=0, column=0, padx=5, pady=5)
    # db_name_entry = tk.Entry(form_window)
    # db_name_entry.grid(row=0, column=1, padx=5, pady=5)

    # table_name_label = tk.Label(form_window, text="Table Name:")
    # table_name_label.grid(row=1, column=0, padx=5, pady=5)
    # table_name_entry = tk.Entry(form_window)
    # table_name_entry.grid(row=1, column=1, padx=5, pady=5)

    column_value_label = tk.Label(form_window, text="UID:")
    column_value_label.grid(row=2, column=0, padx=5, pady=5)
    column_value_entry = tk.Entry(form_window)
    column_value_entry.grid(row=2, column=1, padx=5, pady=5)

    # create a button widget to delete the data
    delete_manual_button = tk.Button(form_window, text="Delete Manual", command=delete_manual)
    delete_manual_button.grid(row=4, column=0, columnspan=2, padx=5, pady=5)

def delete_table(db_click, table_click):

    db_name = db_click.get()
    table_name = table_click.get()
    
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db = db_name,
        charset = 'utf8',
        cursorclass = pymysql.cursors.DictCursor
    )
    cursor = cnx.cursor()
    query = f"DELETE FROM {table_name}"
    cursor.execute(query)

    cnx.commit()
    cursor.close()
    cnx.close()

    form_window.destroy()
    populate_table()

def show_delete_table_form():
    global form_window
    form_window = tk.Toplevel()
    form_window.title("Delete Table")

    question_1_label = tk.Label(form_window, text="Are you sure you want to delete?")
    question_1_label.grid(row=0, column=0, padx=5,pady=5)
    question_2_label = tk.Label(form_window, text="If not close window.")
    question_2_label.grid(row=1, column=0, padx=0, pady=0)

    table_button = tk.Button(form_window, text="Yes", command=delete_table)
    table_button.grid(row=2, column=1, padx=5, pady=5)


