import pymysql
global base 


def connect():
    base = pymysql.connect(
    host = "localhost",
    user = "root",
    passwd = "Idontlikethis_23",
    db = "test",
    charset = 'utf8',
    cursorclass = pymysql.cursors.DictCursor
    )
    return base

def read_database_client(base): 
    print("Reading from client")
    with base.cursor() as cursor:
            # Read data from database
        sql = "SELECT * FROM `client_list`"
        cursor.execute(sql)
            # Fetch all rows
        rows = cursor.fetchall()
            # Print results
        for row in rows:
            print(row)

def read_database_logs(base): 

    with base.cursor() as cursor:
            # Read data from database
        sql = "SELECT * FROM `logs`"
        cursor.execute(sql)
            # Fetch all rows
        rows = cursor.fetchall()
            # Print results
        for row in rows:
            print(row)

def read_database_unknown(base):

    with base.cursor() as cursor:
        sql = "SELECT * FROM `unknown`"
        cursor.execute(sql)
            # Fetch all rows
        rows = cursor.fetchall()
            # Print results
        for row in rows:
            print(row)
           
def delete_all_logs(base):
    with base.cursor() as cursor:
        sql = "DELETE FROM `logs`"
        cursor.execute(sql)
    base.commit()

def delete_all_unknown(base):
    with base.cursor() as cursor:
        sql = "DELETE FROM `unknown` WHERE id != -1"
        cursor.execute(sql)
    base.commit()

def delete_all_client_list(base):
    with base.cursor() as cursor:
        sql = "DELETE FROM `client_list`"
        cursor.execute(sql)
    base.commit()

def delete_all_entries(base):
    show_tables(base)
    table = input("What table would you like to empty? ").lower()
    print(table)
    
    if table == "client_list":
        delete_all_client_list(base)
    if table == "logs":
        delete_all_logs(base)
    if table == "unknown":
        delete_all_unknown(base)
    

def show_tables(base):
    with base.cursor() as cursor:
        sql = "SHOW TABLES"
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            print(row)


def outliers(base):
    with base.cursor() as cursor:
        sql = "SELECT `uid` FROM `logs` WHERE (`counter`) != '1'"
        cursor.execute(sql)
        rows = cursor.fetchall()
        uid = ""
        for row in rows:
            uid = row['uid']
            print(row)
    return uid

def find_name(base, uid):
    with base.cursor() as cursor:
        sql = "SELECT  `name` FROM `client_list` WHERE (`uid`) = %s"
        cursor.execute(sql, (uid))
        rows = cursor.fetchall()
        for row in rows:
            print(row)

def find_outliers(base):
    uid = outliers(base)
    find_name(base, uid)




def main():
    test = connect()
    read_database_client(test)
    read_database_logs(test)
    read_database_unknown(test)

    # find_outliers(test)
    # delete_all_entries(test)

    # read_database_client(test)
    # read_database_logs(test)
    # read_database_unknown(test)


    # show_tables(test)
    test.close()
    print("Exit was successful")

main()