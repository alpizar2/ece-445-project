from nfc_gui import *
import tkinter as tk
from tkinter import ttk
from tkinter import *


delete_all_logs()
delete_all_unknown()
options_table = get_options_table()
options_database = ['test']
button_options = {
    "client_list":["Add Data", "Delete Table","Delete Data","Delete Manual"],
    "logs":["Delete Table", "Liable"],
    "unknown":["Delete Table"]
}

def update_frame(*args):
    current = table_click.get()
    buttons = button_options.get(current, [])

    for button in button_frame.winfo_children():
        button.destroy()

    for button_text in buttons:
        if button_text == "Add Data":
            button = tk.Button(button_frame, text=button_text, command=show_add_data_form)
        if button_text == "Delete Table":
            button = tk.Button(button_frame, text=button_text, command=show_delete_table_form)
        if button_text == "Delete Manual":
            button = tk.Button(button_frame, text=button_text, command=show_delete_manual_form)
        if button_text == "Delete Data":
            button = tk.Button(button_frame, text=button_text, command=show_delete_data_form)
        if button_text == "Liable":
           button = tk.Button(button_frame, text=button_text, command=show_liable_form) 
        button.pack(side="top")
        


# create a new tkinter window
window = tk.Tk()

# set the window title
window.title("SQL Table Viewer")

#dropdown options

table_click = StringVar(window)
table_click.set("Choose Table")

button_frame = tk.Frame(window)
button_frame.grid(row=6, column=6, padx=20, pady=20)
update_frame()

table_click.trace("w", update_frame)


db_click = StringVar()
db_click.set("Choose DB")

# create a label widget for the database name
db_name_label = tk.Label(window, text="Database Name:")
db_name_label.grid(row=0, column=0, padx=5, pady=5)

# create an entry widget for the database name
db_name_entry = tk.OptionMenu(window, db_click, *options_database)
db_name_entry.grid(row=0, column=1, padx=5, pady=5)

# create a label widget for the table name
table_name_label = tk.Label(window, text="Table Name:")
table_name_label.grid(row=1, column=0, padx=5, pady=5)

# create an entry widget for the table name
table_name_entry = tk.OptionMenu(window, table_click, *options_table)
table_name_entry.grid(row=1, column=1, padx=5, pady=5)

# # create a Treeview widget to display the table data
tree = ttk.Treeview(window, columns=("column1", "column2", "column3", "column4", "column5"), show="headings")
tree.grid(row=2, column=0, columnspan=2, padx=5, pady=5)
initial = table_click.get()
# define a function to populate the Treeview widget with data
def populate_table():
    # get the database name and table name from the entry widgets
    db_name = db_click.get()
    # table_name = table_name_entry.get()
    table_name = table_click.get()
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )
    # create a cursor object
    cursor = cnx.cursor()

    # execute a SELECT statement to retrieve the data from the table
    query = f"SELECT * FROM {table_name}"
    cursor.execute(query)

    # get the column names from the cursor description
    column_names = [i[0] for i in cursor.description]

    # set the columns in the Treeview widget
    tree["columns"] = column_names
    for col in column_names:
        tree.heading(col, text=col)

    # clear all items from the treeview widget
    tree.delete(*tree.get_children())
    # populate the Treeview widget with the data
    for row in cursor.fetchall():
        tree.insert("", "end", values=list(row.values()))

    # close the cursor and connection
    cursor.close()
    cnx.close()


# create a button widget to populate the table
populate_button = tk.Button(window, text="Populate Table", command=populate_table)
populate_button.grid(row=3, column=0, padx=5, pady=5)


def show_add_data_form():
    # create a new tkinter window for the form
    global form_window, column2_entry
    form_window = tk.Toplevel(window)

    # set the window title
    form_window.title("Add Data")

    # create a label widget for the first column
    # column1_label = tk.Label(form_window, text="uid:")
    # column1_label.grid(row=0, column=0, padx=5, pady=5)

    # create an entry widget for the first column
    # column1_entry = tk.Entry(form_window)
    # column1_entry.grid(row=0, column=1, padx=5, pady=5)

    # create a label widget for the second column
    column2_label = tk.Label(form_window, text="name:")
    column2_label.grid(row=1, column=0, padx=5, pady=5)

    # create an entry widget for the second column
    column2_entry = tk.Entry(form_window)
    column2_entry.grid(row=1, column=1, padx=5, pady=5)

    # # create a label widget for the third column
    # column3_label = tk.Label(form_window, text="Column 3:")
    # column3_label.grid(row=2, column=0, padx=5, pady=5)

    # # create an entry widget for the third column
    # column3_entry = tk.Entry(form_window)
    # column3_entry.grid(row=2, column=1, padx=5, pady=5)

    # define a function to insert the data into the database
    def insert_data():
        # get the database name and table name from the entry widgets
        db_name = db_click.get()
        table_name = table_click.get()

        # get the data from the entry widgets
        #column1 = column1_entry.get()
        column1 = add_user_nfc()
        print(column1)
        column2 = column2_entry.get()
            #column3 = column3_entry.get()

            # create a connection to the MySQL database
        cnx = pymysql.connect(
            host='localhost',
            user='root',
            password='Idontlikethis_23',
            db=db_name,
            charset='utf8',
            cursorclass=pymysql.cursors.DictCursor
        )

            # create a cursor object
        cursor = cnx.cursor()
        flag = check_db(column1)
        print(flag)
        if(check_db(column1) == "FALSE"):
            
            # execute an INSERT statement to insert the data into the table
            query = f"INSERT INTO {table_name} (uid, name) VALUES (%s, %s)" #, column3 , %s
            values = (column1, column2) #, column3)
            cursor.execute(query, values)
        if(check_db(column1) == "TRUE"):
            exist_window = tk.Tk()
            exist_window.title("User Exists")
            # exist_window.grid(row=2, column=0, padx=5, pady=5)
            insert_label = tk.Label(exist_window, text="User already in system")
            insert_label.grid(row=0, column=0, padx=5, pady=5)
            insert_label_2 = tk.Label(exist_window, text="Please close window")
            insert_label_2.grid(row=1, column=0, padx=5, pady=5)

        # commit the transaction and close the cursor and connection
        cnx.commit()
        cursor.close()
        cnx.close()

        # close the form window
        form_window.destroy()
        # exist_window.destroy()

        #tk.update()
        # repopulate the table with the new data
        populate_table()

    # create a button widget to insert the data
    insert_button = tk.Button(form_window, text="Scan NFC", command=insert_data)
    insert_button.grid(row=3, column=0, columnspan=2, padx=5, pady=5)

# create a button widget to show the add data form
# add_data_button = tk.Button(window, text="Add Data", command=show_add_data_form)
# add_data_button.grid(row=3, column=1, padx=5, pady=5)

def delete_data():
    # get the database name, table name, and column value from the entry widgets
    db_name = db_click.get()
    table_name = table_click.get()
    #column_value = column_value_entry.get()
    #print(column_value)
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    # create a cursor object
    cursor = cnx.cursor()
    chip = add_user_nfc()
    # execute a DELETE statement to delete the data from the table
    query = "DELETE FROM `client_list` WHERE uid= %s"
    cursor.execute(query, (chip))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    cursor.close()
    cnx.close()

    # close the form window
    form_window.destroy()

    # repopulate the table with the updated data
    populate_table()

# define a function to show the delete data form
def show_delete_data_form():
    global form_window

    # create a new window for the form
    form_window = tk.Toplevel()
    form_window.title("Delete Client")

    # create labels and entry widgets for the form
    # db_name_label = tk.Label(form_window, text="Database Name:")
    # db_name_label.grid(row=0, column=0, padx=5, pady=5)
    # db_name_entry = tk.Entry(form_window)
    # db_name_entry.grid(row=0, column=1, padx=5, pady=5)

    # table_name_label = tk.Label(form_window, text="Table Name:")
    # table_name_label.grid(row=1, column=0, padx=5, pady=5)
    # table_name_entry = tk.Entry(form_window)
    # table_name_entry.grid(row=1, column=1, padx=5, pady=5)

    column_value_label = tk.Label(form_window, text="Scan NFC of user to delete.")
    column_value_label.grid(row=2, column=0, padx=5, pady=5)
    # column_value_entry = tk.Entry(form_window)
    # column_value_entry.grid(row=2, column=1, padx=5, pady=5)

    # create a button widget to delete the data
    delete_button = tk.Button(form_window, text="Delete Data", command=delete_data)
    delete_button.grid(row=3, column=0, columnspan=2, padx=5, pady=5)


# create a button widget to show the delete data form
# delete_data_button = tk.Button(window, text="Delete Data", command=show_delete_data_form)
# delete_data_button.grid(row=3, column=2, padx=5, pady=5)

def delete_manual():
    # get the database name, table name, and column value from the entry widgets
    db_name = db_click.get()
    # table_name = table_name_entry.get()
    column_value = column_value_entry.get()
    #print(column_value)
    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    # create a cursor object
    cursor = cnx.cursor()
    # execute a DELETE statement to delete the data from the table
    query = f"DELETE FROM `client_list` WHERE uid=%s"
    cursor.execute(query, (column_value))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    cursor.close()
    cnx.close()

    # close the form window
    form_window.destroy()

    # repopulate the table with the updated data
    populate_table()

def show_delete_manual_form():
    global column_value_entry, form_window

    # create a new window for the form
    form_window = tk.Toplevel()
    form_window.title("Delete Client.")

    # create labels and entry widgets for the form
    # db_name_label = tk.Label(form_window, text="Database Name:")
    # db_name_label.grid(row=0, column=0, padx=5, pady=5)
    # db_name_entry = tk.Entry(form_window)
    # db_name_entry.grid(row=0, column=1, padx=5, pady=5)

    # table_name_label = tk.Label(form_window, text="Table Name:")
    # table_name_label.grid(row=1, column=0, padx=5, pady=5)
    # table_name_entry = tk.Entry(form_window)
    # table_name_entry.grid(row=1, column=1, padx=5, pady=5)

    column_value_label = tk.Label(form_window, text="UID:")
    column_value_label.grid(row=2, column=0, padx=5, pady=5)
    column_value_entry = tk.Entry(form_window)
    column_value_entry.grid(row=2, column=1, padx=5, pady=5)

    # create a button widget to delete the data
    delete_manual_button = tk.Button(form_window, text="Delete Manual", command=delete_manual)
    delete_manual_button.grid(row=4, column=0, columnspan=2, padx=5, pady=5)


# create a button widget to show the delete data form
# delete_manual_data_button = tk.Button(window, text="Delete Manual", command=show_delete_manual_form)
# delete_manual_data_button.grid(row=4, column=2, padx=5, pady=5)


def delete_table():

    db_name = db_click.get()
    table_name = table_click.get()
    
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db = db_name,
        charset = 'utf8',
        cursorclass = pymysql.cursors.DictCursor
    )
    cursor = cnx.cursor()
    query = f"DELETE FROM {table_name}"
    cursor.execute(query)

    cnx.commit()
    cursor.close()
    cnx.close()

    form_window.destroy()
    populate_table()


def show_delete_table_form():
    global form_window
    form_window = tk.Toplevel()
    form_window.title("Delete Table")

    question_1_label = tk.Label(form_window, text="Are you sure you want to delete?")
    question_1_label.grid(row=0, column=0, padx=5,pady=5)
    question_2_label = tk.Label(form_window, text="If not close window.")
    question_2_label.grid(row=1, column=0, padx=0, pady=0)

    table_button = tk.Button(form_window, text="Yes", command=delete_table)
    table_button.grid(row=2, column=1, padx=5, pady=5)

def show_liable_form():
    global form_window, liable_tree
    form_window = tk.Toplevel()
    form_window.title("Liable Table")

    text_label = tk.Label(form_window, text="These Clients are Liable")
    text_label.grid(row=0, column =0, padx=5, pady=5)

    liable_tree = ttk.Treeview(form_window, columns=("uid", "name", "time"), show="headings")
    liable_tree.grid(row=2, column=0, columnspan=2, padx=5, pady=5)

    column_names = ['uid', 'name', 'time']
    liable_tree["columns"] = column_names
    for col in column_names:
        liable_tree.heading(col, text=col)


    text_button = tk.Button(form_window, text="Populate Liable", command=populate_liable)
    text_button.grid(row=3, column=1, padx=5, pady=5)

def populate_liable():
    uid_list, name_list, timer_list = get_logs_liable()

    for i in range(len(uid_list)):
        liable_tree.insert("","end", values=[uid_list[i], name_list[i], timer_list[i]])



def get_logs_liable():
    cnx = pymysql.connect(
        host = 'localhost',
        user='root',
        password = 'Idontlikethis_23',
        db='test',
        charset='utf8',
        cursorclass=pymysql.cursors.DictCursor
    )

    cursor = cnx.cursor()

    query = "SELECT `uid` FROM `logs` WHERE counter > 1"
    #query = "SELECT * FROM `client_list`"
    cursor.execute(query)

    result = cursor.fetchall()
    complete = {}
    for row in result:
        temp = row['uid']
        if temp not in complete.keys():
            complete[temp] = {}

    for uid in complete.keys():
        new_query = "SELECT `name` FROM `client_list` WHERE uid=%s"
        cursor.execute(new_query, (uid))
        result = cursor.fetchall()
        for row in result:
            temp = row['name']
            if temp not in complete[uid]:
                complete[uid][temp] = {}



    for uid in complete.keys():
        new_query = "SELECT `time` FROM `logs` WHERE uid=%s"
        cursor.execute(new_query, (uid))
        result = cursor.fetchall()
        for name in complete[uid].keys():
            time_list = []
            for row in result:
                time_list.append(row['time'])
            complete[uid][name] = time_list
    print(complete)

    cursor.close()
    cnx.close()
    uid_list = []
    name_list = []
    timer_list = []
    for uid in complete.keys():
        print(uid)
        for name in complete[uid].keys():
            print(name)
            temp = len(complete[uid][name])
            print(temp)
            for time in complete[uid][name]:
                print(time)
                timer_list.append(time)
                uid_list.append(uid)
                name_list.append(name)
    return uid_list, name_list, timer_list


# start the tkinter event loop
window.mainloop()
