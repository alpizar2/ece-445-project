"""
This example shows connecting to the PN532 with I2C (requires clock
stretching support), SPI, or UART. SPI is best, it uses the most pins but
is the most reliable and universally supported.
After initialization, try waving various 13.56MHz RFID cards over it!
"""

import RPi.GPIO as GPIO


from pn532 import *
def addZero(x):
    length = len(x)
    if length == 1:
        x = '0'+x
        return x
    else:
        return x

if __name__ == '__main__':
    try:
        #pn532 = PN532_SPI(debug=False, reset=20, cs=4)
        #pn532 = PN532_I2C(debug=False, reset=20, req=16)
        pn532 = PN532_UART(debug=False, reset=20)

        ic, ver, rev, support = pn532.get_firmware_version()
        print('Found PN532 with firmware version: {0}.{1}'.format(ver, rev))

        # Configure PN532 to communicate with MiFare cards
        pn532.SAM_configuration()

        print('Waiting for RFID/NFC card...')
        flag = True
        fuid = ''
        fhex = ''
        while flag:
            # Check if a card is available to read
            uid = pn532.read_passive_target(timeout=0.5)


            print('.', end="")
            # Try again if no card is available.
            if uid is None:
                continue
            for i in uid:
                newi = addZero(str(i))
                fuid += newi    
            print(type(uid))
            temp = [hex(i) for i in uid]
            print('Found card with UID:', temp)

            for b in temp:
                len_of_uid = len(b)
                if len_of_uid == 3:
                    hex_temp = "0"+b[2]
                
                if len_of_uid == 4:
                    hex_temp = b[2] + b[3]
                
                fhex += hex_temp

                
            print('Found card with UID: {}'.format(uid))
            print('Found card with UID: {}'.format(fuid))
            print(fhex)
            break
           
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()

