#include <LiquidCrystal_I2C.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <SPI.h>
#include <MFRC522.h>
#include "time.h"
#include "sntp.h"

#define RST_PIN 16
#define SS_PIN 19

#define SPI_SCK 18
#define SPI_MISO 17
#define SPI_MOSI 5
#define SPI_CS 19

//location of subcomponent
const String location = "door_1";

//create object for Reader and Display
MFRC522 mfrc522(SS_PIN, RST_PIN);
LiquidCrystal_I2C lcd(0x27, 16, 2);

//variables for ultrasonic sensor
const int trig = 15; 
const int echo = 2;
long duration;
int distance;
int length;

//variables for LEDs
const int GREEN_PIN = 12;
const int RED_PIN = 14;

//variables for HTTP
String True = "True";
String False = "False";

//variables for Cache
String cache[1000];
String empty = "empty";
unsigned long timer1_start, timer1_end, timer1_delta;
unsigned long timer2_start, timer2_end, timer2_delta;

//network credentials
const char* ssid  = ""; // use wifi name
const char* password = ""; // use password

//time credentials
const char* ntpServer1 = "pool.ntp.org";
const char* ntpServer2 = "time.nist.gov";
const char* time_zone = "CST6CDT,M3.2.0,M11.1.0";

String Time = "";


//http information
String host = "http://192.168.235.191:80/php_scripts";
String exist = "/exist.php";
String add = "/add.php";
String unknown = "/unknown.php";
String question = "?";
String plus = "&";
String param1 = "name=";
String param2 = "value=";
String param3 = "location=";
String param4 = "counter=";
String param5 = "uid=";
String param6 = "time=";

//variables used to communicate information
String UID = "";

//variables to verify speed

void setup() {
  Serial.begin(115200);

  while(!Serial);
  SPI.begin(SPI_SCK, SPI_MISO, SPI_MOSI, SPI_CS);
  mfrc522.PCD_Init();

  lcd.init();
  lcd.backlight();

  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);

  pinMode(GREEN_PIN, OUTPUT);
  pinMode(RED_PIN, OUTPUT);

  //time stuff
  configTzTime(time_zone, ntpServer1, ntpServer2);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  lcd.setCursor(0, 0);
  lcd.print("WiFi Connected");
  delay(1000);
  lcd.clear();

  clear_cache();
  length = calibration();
  Serial.print("pathway: ");
  Serial.println(length);
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    if ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial() ) {
      return;
    }

    UID = "";
    for (uint8_t i = 0; i < mfrc522.uid.size; i++) {
      UID.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? "0" : ""));
      UID.concat(String(mfrc522.uid.uidByte[i], HEX));
    }

    int index;

    //check if inside cache
    timer1_start = millis();
    bool cache_flag = in_cache(UID);
    timer1_end = millis();
    timer1_delta = timer1_end - timer1_start;
    // Serial.print("Cache time: ");
    // Serial.println(timer1_delta);
    if (cache_flag) {
      green_light();
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Access Granted");
      delay(1000);
      //Serial.println("In Cache.");
      lcd.clear();
      ultrasonic();
      lcd.setCursor(0, 0);
      lcd.print("Cache Time: ");
      lcd.setCursor(0, 1);
      lcd.print(timer1_delta);
      delay(2000);
      lcd.clear();
      // Serial.print("Cache time: ");
      // Serial.println(timer1_delta);
    }
    //if not in cache
    else {
      // check if in database
      timer2_start = millis();
      bool database_flag = in_database(UID);
      timer2_end = millis();
      timer2_delta = timer2_end - timer2_start;
      // Serial.print("Database time: ");
      // Serial.println(timer2_delta);
      if (database_flag == true) {
        green_light();
        lcd.setCursor(0, 0);
        lcd.print("Access Granted");
        delay(2000);
        lcd.clear();
        //Serial.println("In database");

        //if not in cache add to cache
        if (!in_cache(UID)) {
          //actual code
          index = free_index();
          if (index != -1) {
            cache[index] = UID;
          }

          //worst case scenario
          // cache[99] = UID;
        }
        //Serial.println("added to cache.");
        ultrasonic();
        lcd.setCursor(0, 0);
        lcd.print("Database Time: ");
        lcd.setCursor(0, 1);
        lcd.print(timer2_delta);
        delay(2000);
        lcd.clear();

      }
      // if not in database
      else {
        red_light();
        lcd.setCursor(0,0);
        lcd.print("Access Denied");
        delay(2000);
        lcd.clear();
        send_unknown();
        //Serial.println("not in database.");
      }
    }
  }
  // if not connected to WiFi
  else {
    lcd.setCursor(0, 0);
    lcd.print("No WiFi");
    delay(2000);
    lcd.clear();
  }
  delay(2000);
}

bool in_database(String UID) {
  HTTPClient http;
  http.begin(host + exist + question + param1 + UID);
  Serial.println(host + exist + question + param1 + UID);

  int httpcode = http.GET();
  if (httpcode > 0) {
    String outcome = http.getString();
    Serial.println(outcome);
    if (outcome == True){
      http.end();
      return true;
    }
    else {
      http.end();
      return false;
    }
  } 
  // else {
  //   Serial.print("Error code: ");
  //   Serial.println(httpcode);
  // }
  http.end();
  return false; 
}

void send_count (int person_counter) {
  String count = String(person_counter);
  Time = "";
  printLocalTime();
  HTTPClient http;
  http.begin(host + add + question + param5 + UID + plus + param3 + location + plus + param4 + count + plus + param6 + Time);
  Serial.println(host + add + question + param5 + UID + plus + param3 + location + plus + param4 + count + plus + param6 + Time);

  int httpcode = http.GET();
  if (httpcode > 0) {
    String outcome = http.getString();
    //Serial.println(outcome);
  } 
  // else {
  //   //Serial.print("Error code: ");
  //   //Serial.println(httpcode);
  // }
  http.end();
}

void send_unknown () {
  HTTPClient http;
  Time = "";
  printLocalTime();
  http.begin(host + unknown + question + param5 + UID + plus + param3 + location + plus + param6 + Time);
  Serial.println(host + unknown + question + param5 + UID + plus + param3 + location + plus + param6 + Time);

  int httpcode = http.GET();
  if (httpcode > 0) {
    String outcome = http.getString();
    //Serial.println(outcome);
  } 
  // else {
  //   //Serial.print("Error code: ");
  //   //Serial.println(httpcode);
  // }
  http.end();  
}

void red_light() {
  digitalWrite(RED_PIN, HIGH);
  delay(2000);
  digitalWrite(RED_PIN, LOW);
}

void green_light() {
  digitalWrite(GREEN_PIN, HIGH);
  delay(2000);
  digitalWrite(GREEN_PIN, LOW);
}
/*
Function that is ran during setup to calculate the distance of width of the hallway 
where sensor will be located.

Returns: Width of hallway. 

*/
int calibration() {
    digitalWrite(trig, LOW);
    //delay(22);
    delayMicroseconds(2);

    digitalWrite(trig, HIGH);
    //delay(44);
    delayMicroseconds(5);
    digitalWrite(trig, LOW);
    pinMode(echo, INPUT);
    duration = pulseIn(echo, HIGH);
  
    // Convert the time into a distance
    distance = duration * 0.034 / 2;

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(distance);
    delay(2000);
    lcd.clear();

    if (distance > 120) {
      return distance - 20;
    }

    if (distance < 120) {
      return distance - 10; 
    }
}

void ultrasonic() {
    unsigned long time = 0;
    int person_counter = 0;
    int counter = 0; 
    unsigned long start, end, delta;
    int start_counter, final_counter, delta_counter;
    boolean check;
    boolean detect = false;
    do {
        start = millis();
        check = human_detect();
        end = millis();



        if (check == true && detect == false) {
            //detecting a person for the first time
            detect = true;
            start_counter = counter;
        }

        if (check == false && detect == true) {
            // person is done walking 
            final_counter = counter;
            delta_counter = final_counter-start_counter;
            Serial.print("Delta Counter: ");
            Serial.println(delta_counter);
            if (delta_counter < 400) {
              detect = true;
            }
            if (delta_counter > 400) {
              detect = false; 
              person_counter += 1;
            }
        }


        delta = end - start;
        time += delta;
        counter += 1;

    } while (time < 5000);

    Serial.println(time);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Person Counter: ");
    lcd.setCursor(0,1);
    lcd.print(person_counter);
    send_count(person_counter);
}

boolean human_detect() {
    digitalWrite(trig, LOW);
    //delay(22);
    delayMicroseconds(2);

    digitalWrite(trig, HIGH);
    //delay(44);
    delayMicroseconds(5);
    digitalWrite(trig, LOW);
    pinMode(echo, INPUT);
    duration = pulseIn(echo, HIGH);
  
    // Convert the time into a distance
    distance = duration * 0.034 / 2;

    if (distance < length) {
      return true;
    }

    if (distance > length) {
      return false;
    }
}

void clear_cache() {
  for (int i = 0; i < 100; i++) {
    cache[i] = empty;
  }
}

int free_index() {
  for (int i=999; i > -1; i--) {
    if (cache[i] == empty) {
      return i;
    }
    else {
      continue;
    }
  }
  return -1;
}

bool in_cache(String UID) {
  for (int i = 0; i < 1000; i++){
    if (cache[i] == UID) {
      return true;
    }
    else {
      continue;
    }
  }
  return false; 
}

void printLocalTime() {
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("No time available (yet)");
    return;
  }
  String hour = String(timeinfo.tm_hour);
  String min = String(timeinfo.tm_min);
  String sec = String(timeinfo.tm_sec);
  String colon = ":";

  Time.concat(hour + colon + min + colon + sec);
}

