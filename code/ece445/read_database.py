import pymysql

base = pymysql.connect(
    host='localhost',
    user='root',
    password='Idontlikethis_23',
    db='users',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

try:
    with base.cursor() as cursor:
        # Read data from database
        sql = "SELECT * FROM `creditINFO`"
        cursor.execute(sql)

        # Fetch all rows
        rows = cursor.fetchall()

        # Print results
        for row in rows:
            print(row)
finally:
    base.close()