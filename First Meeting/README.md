*High Level-Requirements
- Our NFC bands need to detect when someone with/without access attempts to pass through checkpoints via LEDs, and store this information in a central database that gets cleared on a semi-regular basis.
- Our entry checking mechanism needs to accurately count people who enter, and alert when someone follows another person without tapping their NFC band, this will be done via ultrasonic sensors.
- Our NFC bands need to be linked to a central database linked to user accounts, to which food stalls can send the appropriate billing.
