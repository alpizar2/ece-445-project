# Brennan's ECE445 Lab Notebook

## Date: [02-07-2023]

### Initial Meeting with TA

#### Objective:
First interaction with TA to convey our Idea and set deadlines as well as weekly meetings


## Date: [02-23-2023]

### Brainstorming Part orders for project

#### Objective:
We were to figure out exactly what parts we were going to use for the project. We knew that we were going to use a [Raspberry Pi](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/) along with a [NFC Hat for the Raspberry Pi](https://core-electronics.com.au/nfc-hat-for-raspberry-pi-pn532.html) to use to read the write to the [Wristbands](https://www.amazon.com/YARONGTECH-NTAG215-Wristbands-Compatible-NFC-Enabled/dp/B09N3C1G39/ref=sr_1_1?crid=9DFBK8PTMPGL&keywords=nfc%2Breprogrammable%2Bnfc%2Bwristbands&qid=1677194904&sprefix=nfc%2Breprogrammable%2Bnfc%2Bwristbands%2Caps%2C90&sr=8-1&th=1)

## Date: [04-04-2023]

### Creating the server and the Tables alongside with it

#### Objective:

Created an inital table which houses the Client information called Credit Info
![Image](/notebooks/brennan/images/creditInfo.png)

Also created preliminary tables such as user_logs to work with.

![Image](/notebooks/brennan/images/tables.png)

These tables would be stored on the Raspberry Pi as a SQL Server.

## Date: [04-20-2023]

### Initially creating the GUI and linking the database

#### Objective:

Created a simple GUI table to connect to the MySQL server

![Image](/notebooks/brennan/images/GUI1.png)

This table would be used to navigate the SQL Server since it would be difficult to use the terminal in a Real world application.


The GUI was made with a python extension [Tkinter](https://docs.python.org/3/library/tk.html)
Here is some of the code that was generated in order to make this:

```
import tkinter as tk
from tkinter import ttk
import pymysql.cursors

# create a new tkinter window
window = tk.Tk()

# set the window title
window.title("SQL Table Viewer")

# create a label widget for the database name
db_name_label = tk.Label(window, text="Database Name:")
db_name_label.grid(row=0, column=0, padx=5, pady=5)

# create an entry widget for the database name
db_name_entry = tk.Entry(window)
db_name_entry.grid(row=0, column=1, padx=5, pady=5)

# create a label widget for the table name
table_name_label = tk.Label(window, text="Table Name:")
table_name_label.grid(row=1, column=0, padx=5, pady=5)

# create an entry widget for the table name
table_name_entry = tk.Entry(window)
table_name_entry.grid(row=1, column=1, padx=5, pady=5)

# create a Treeview widget to display the table data
tree = ttk.Treeview(window, columns=("column1", "column2", "column3"), show="headings")
tree.grid(row=2, column=0, columnspan=2, padx=5, pady=5)

# define a function to populate the Treeview widget with data
def populate_table():
    # get the database name and table name from the entry widgets
    db_name = db_name_entry.get()
    table_name = table_name_entry.get()

    # create a connection to the MySQL database
    cnx = pymysql.connect(
        host='localhost',
        user='root',
        password='Idontlikethis_23',
        db=db_name,
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )
    # create a cursor object
    cursor = cnx.cursor()

    # execute a SELECT statement to retrieve the data from the table
    query = f"SELECT * FROM {table_name}"
    cursor.execute(query)

    # get the column names from the cursor description
    column_names = [i[0] for i in cursor.description]

    # set the columns in the Treeview widget
    tree["columns"] = column_names
    for col in column_names:
        tree.heading(col, text=col)

    # populate the Treeview widget with the data
    for row in cursor.fetchall():
        tree.insert("", "end", values=list(row.values()))

    # close the cursor and connection
    cursor.close()
    cnx.close()


# create a button widget to populate the table
populate_button = tk.Button(window, text="Populate Table", command=populate_table)
populate_button.grid(row=3, column=0, padx=5, pady=5)
```

What I have shown is just code for the populate table button as well as the main window

## Date: [04-23-2023]

### Making progress on the UI as well as figurig out PHP Server Admin processes to connect the microcontroller

Used this [Link for Raspberry Pi Apache MySQL PHP installation](https://randomnerdtutorials.com/raspberry-pi-apache-mysql-php-lamp-server/)


## Date: [04-25-2023]

### Finalized functionality to the GUI for the Server


More functionality to show more tables and more columns to include more information. At this point we have 3 main tables which are: Client List, Logs, and Unknown. Client list logs in all the information for Client information, the Logs records all valid interactions, and the unknown table records all invalid information.

![Image](/notebooks/brennan/images/GUI2.jpg)

Also added an SCANNFC that edson coded to enable a read from the NFC Hat on the raspberry PI and automatically load the data into the Server

![Image](/notebooks/brennan/images/SCANNFC.jpg)

