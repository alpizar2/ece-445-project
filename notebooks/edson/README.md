# Edson's Notebook
* [Edson's Notebook](#Edson's-Notebook)
* [2023-02-07 - First Meeting with TA](#2023-02-07-First-Meeting-with-TA)
* [2023-02-16 - Writing Down Ideas](#2023-02-16-Writing-Down-Ideas)
* [2023-03-13 - Building Server Subsystem](#2023-03-13-Building-Server-Subsystem)
* [2023-03-15 - Building the Reading/Writing Subsystem](#2023-03-15-Building-the-Reading/Writing-Subsystem)
* [2023-03-17 - Combining Server and Reader/Writing Subsystem](#2023-03-17-Combining-Server-and-Reading/Writing-Subsystems)
* [2023-03-26 - Starting the code for the sensor Subsystem](#2023-03-26-Starting-the-code-for-the-sensor-Subsystem)
* [2023-04-03 - Almost completed Sensor subsystem](#2023-04-03-Almost-completed-Sensor-subsystem)
* [2023-04-08 - Sensor algorithm improvements and ideas](#2023-04-08-Sensor-algorithm-improvements-and-ideas)
* [2023-04-09 - Finalizing Ultrasonic algorithm](#2023-04-09-Finalizing-Ultrasonic-algorithm)
* [2023-04-15 - Sensor accuracy is improved](#2023-04-15-Sensor-accuracy-is-improved)
* [2023-04-18 - Mock Demo](#2023-04-18-Mock-Demo)
* [2023-04-20 - Initial ESP32 tests](#2023-04-20-Initial ESP32 tests)
* [2023-04-22 - PHP and Web Server Progress](#2023-04-22-PHP-and-Web-Server-Progress)
* [2023-04-23 - HTTP Requests work](#2023-04-23-HTTP-Requests-work)
* [2023-04-24 - Finalizing GUI](#2023-04-24-Finalizing GUI)
* [2023-04-25 - Final Changes and PCB work](#2023-04-25-Final-Changes-and-PCB-work)
* [2023-04-26 - Our Final Demo](#2023-04-26-Our-Final-Demo)



# 2023-02-07 First Meeting with TA
We discussed our initial ideas with the TA. We learned about areas where our project might cause issues. We got feedback on our first draft of our block diagram and we got suggestions on how to improve it.

# 2023-02-16 Writing Down Ideas 
After our first meeting I began to think about the solution we would implement for the project and what I would be responsible for. Since I own the Raspberry Pi my intial thoughts would be to work on the creating an SQL server onto the pi. Our group still has not finalized what parts we would be using for our reader system so I have not thought about the algorithm we could use to get the results we want. I have also acquired an Arduino Uno that we could use to do basic testing with the components that we would eventually buy.  

# 2023-03-13 Building Server Subsystem
I began to build the SQL database onto the Raspberry Pi. I had to download libraries onto the Pi and make sure that they worked. I created a database and created a table that printed out the contents I wrote as a way to show that the database was behaving correctly. I also began to write python scripts that worked with SQL commands so that modifying the database would be much easier. I used the following link to create the database: https://pimylifeup.com/raspberry-pi-mysql/. Once I got accustumed to the SQL conding syntax I was able to begin to write commands that would allow us to add new users onto the database. I also learned that you can write python scripts that can connect to an SQL server and have functions that run SQL commands.The image below shows the inital tables that I made onto the server.

![initial_table](/uploads/517debc46b6c0078009a1f34d63ba716/initial_table.PNG)

The script belows allows me to read all the entries from the client_list table from our database.  

![image](/uploads/f9ffcc4d020593c6aac3132a0b8f6f29/image.png)

# 2023-03-15 Building the Reading/Writing Subsystem
After obtaining the NFC reader/writer kit for the Pi I installed it and began to write code that would read an NFC tag whenever I scanned it. The issue with this is that it took me a while to realize that the UID in the NFC tags were written as bytes. For this I had to write code to translate the bytes as a single HEX array. Once I figured this I worked on writing information onto an NFC tag but this proved to be a lot more difficult then initially thought. The code needed to read NFC tags was heavily inspired by the example code that the website for the NFC kit had. Their website has a zip file that can be downloaded which containts the libraries that allow the NFC kit to communicate to the raspberry pi. The following code allows me to print out the UID of the NFC tag.

![image](/uploads/d83d2d20df554307ab4460e23f2649ec/image.png)

Since the UID that is read is stored in bytes I have to convert the list of bytes into a list of hex values that would I would then convert into a single string variable. 

# 2023-03-17 Combining Server and Reading/Writing Subsystems
I began to combine the reader/writer and server subsystems. My inital goal was to be able to read a UID whenever one was scanned and add that UID onto a database where the user would be prompted with additional information to add. To do this I had to write python scripts that would use SQL commands to accomplish these tasks. After completing those scripts I was successful to print out the information stored onto a database and add information onto a database whenever a new UID was scanned if the user wanted it. 

![image](/uploads/4d88f4cf35a6de26ce2bb926cdf8957a/image.png)

When running the script the user would be asked questions on whether they would like to modify a table in the database by either adding or deleting an entry from the table. The way I wrote the code made it so that if the option to add a user was selected the NFC reader will enter a while loop that will continuesly try to read an NFC tag and would only exit the while loop if the UID was read or if the UID was corrupted. 


# 2023-03-18 New Idea for Server
I realized that it would be a great idea to have a GUI so that the user does not have to type on the terminal. The whole day I looked for easy GUIs that I could implement and this is when I learned about the tkinter library that exists in Python. I did not get very far in the GUI process. 

# 2023-03-26 Starting the code for the sensor Subsystem
After acquiring the individual parts for the sensor subsystem I began to write individual code for the LEDS and and Ultrasonic sensor. I did not accomplish much this day. I was only able to display the distance the Ultrasonic sensor detected and light the red and green LEDs correctly.
Since this was my first time I had to write anytype of code for an Arduino I had to look up tutorials on how to write code that would read a distance using an ultrasonic sensor. I used the following tutorial, https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/, to get a feeling on how to write code on the Arduino IDE and to also scope how the ultrasonic sensor works. After getting the ultrasonic sensor to read distances I began to brainstorm how I would modify the code I had such that the ultrasonic sensor would only be on for a period of 5 seconds at a time.  

# 2023-04-03 Almost completed Sensor subsystem
Since I still did not have access to a PCB with a microcontroller I instead did all of my individual testing with an Arduino. With the Arduino I was able to write code that behaved according to whether or not a NFC tag is registered onto the database. If the tag was in the database the LED would light green and display it on the Display with its UID. It would then register the user onto the Arduino memory. If the user was not registered then the red LED would light up and display on the display "Access Denied". The database in this case was not actually the database held on the raspberry pi but instead it was an array of strings. The reason why it isn't the actual SQL database is because I dont have access to the microcontroller with wireless capabilities. I was also able to store a UID on the cache of the microcontroller whenever it was read for the first time. The flowchart I used to write my algorithm was the following: 

![image](/uploads/c20251428e10cf7bf270e53f385fcf14/image.png)

The image below shows how I was set up a mock database onto the arduino microcontroller to test whether a NFC tag was registered or not and thus use the LEDS to visually show whether or not a user was registered in the database or not. At this point my biggest issue was getting the ultrasonic sensor to behave how I wanted it to. The reason for this is because I haven't figured out how to maintain the ultrasonic on for 5 seconds because currently when I run the ultrasonic algorithm it will only run once and not function properly because a single run of the ultrasonic sensor takes about 12 microseconds.  

![image](/uploads/eac16637a7e681113f5707cacc11950f/image.png)

# 2023-04-08 Sensor algorithm improvements and ideas 
I was very busy during these days but I did spend alot of time thinking about how to get ultrasonic to detect people. I originally thought about having a helper function that would be an infinite while loop but I could not think of a way to break the while loop after the function ran for 5 seconds. During this time I asked friends who had experience with Arduino IDE if they knew how to do interreputs on an Arduino. I did get many suggestions but many of them were very complicated solutions. The solutions were also not relavent to what I was trying to accomplish. This is where I learned about the millis function. With this function I could the time it took for a function to run since I can set the first millis at the beginning of the function when I first call the function and set the second millis at the end of the function and take the difference of the return values to see how long the function took. Although I made this discovery I still had to proper way to terminate the infinite while loop that ran the ultrasonic sensor. 

![image](/uploads/676ece297f1064fb681bfa057dec71b2/image.png)

In the code shown above I show how I use two variables to determine the time it takes my "person" function to run. By setting the starting point before the function runs I can take the difference with end point to figure out how long in milliseconds the "person" function ran for. 

# 2023-04-09 Finalizing Ultrasonic algorithm

On this day I gave Brennan, my teammate, the raspberry pi so he could work on the SQL gui. I then began to finalize the ultrasonic code. I leared that there exists "do while" loops in c/c++. This type of loop was what I was looking for because I could finally have the ultrasonic sensor turn on for a period of 5 seconds. The code below illustrates that. 

![image](/uploads/6561755ed4ca8480c06a11e5461c6730/image.png)

As you can see via the image the do while loop will make the ultrasonic sensor function runs atleast once and will continue to do run as long as the time variable does not exceed 5000 milliseconds or 5 seconds. Once I figured out this logic I finalized all the code that would run make our system run accordingly. 

Right away I noticed that when a person who had access would walk by the ultrasonic the counter would not display the correct number. When a person walks by the ultrasonic sensor it should only count one person but for some reason it would count 1000, sometimes even more.

# 2023-04-15 Sensor accuracy is improved

When I did testing the ultrasonic sensor was positined at a distance of around 80 centimeters away from a wall. My algorithm to detect people used a two flag system. One of the flags was a boolean variable that would be used to keep track of the return value of my human detect function as shown below:

![image](/uploads/0ab1efb1f5ced8535af2b907b12c5c1a/image.png)

The other flag would be used to keep track of when a person was first detected as shown below:

![image](/uploads/84560652193c7446017267d8460291dd/image.png)

Initially both my check and detect flag would be false but once a person was first detected both check and detect would become true. Once a person was not detected then the detect flag would be change to false and this is when the person counter would increment. An issue that I had when I ran some tests was that the sensor was still not being accurate. I decided to have print statements that would print my check and detect flags to see how they changed while a person walked by. I realized that the ultrasonic was very sensitive and that sometimes it would detect the gap between a person's legs when they walked as two or more people. The clever way I was able to solve this issue is by having a conditional statement that would only increase the person counter when the check flag was true and the detect flag was false after 400 iterations. If the flags met those conditions and function had not run for 400 iterations then the counter would not increase. 

![image](/uploads/2b82507b5fe92d59f3790376b145c9a3/image.png)

After this change the ultrasonic was behaving accordingly and all the components on the breadboard were working as intended. 

# 2023-04-18 Mock Demo
During the mock demo we demoed our breadboard implimitation of our project since we had included our ESP32 microcontroller onto the overall design. It worked as inteded which meant that it was able to distinguish whether a NFC tag was in our database or not and count the correct number of people when they walked by the ultrasonic sensor. The only issue we noticed is that ultrasonic sensor had to face a wall if it did not then it would not be able to count people correctly. Today I acquired a dev module of our microcontroller. 

# 2023-04-20 Inital ESP32 tests
I spent most of the day figuring out how to connect the ESP32 to the internet. I had to use my mobile hotspot to do since the university wifi would not appear on the search list. On this same day Ege and I were able to get our breadboard implementation of our reader/sensor system working with a ESP32 rather than an Arduino Uno. 

# 2023-04-22 PHP and Web Server Progress
Since I did not have access to the raspberry pi since Brennan was working on the GUI I had to install a program that would install a web server, sql server, and allow me to run PHP scripts on my laptop. I decided to use an app called xampp sinse it included all these programs. I spent the majority of the day learning and writing PHP scripts that would be used to modify data from our board system onto the raspberry pi. I first started by making a script where I could I could return a true or false value on whether or name a UID existed in the database. The script is as follows:

![image](/uploads/12a7c3b0be7b3f5367444bde7d9e6c4c/image.png)

Surprisingly the script worked on my first test run. I had to manually add entries onto our client_list table since I did not the raspberry pi with the nfc hat add on but nevertheless it worked. I then made two more scripts that would add data to our logs table and our unknown table. These scripts I could not get to test until I had HTTP requests working. 

# 2023-04-23 HTTP Requests work
On this day I spent most of the day working on understanding and implement HTTP requests. I found a tutorial online, https://randomnerdtutorials.com/esp32-http-get-post-arduino/, that a decent job at explaining how to implement this. I learned that I need to use GET requests so that I could information from the web/sql server and to also be able to modify data. I had to modify my php requests to include the GET request so that the HTTP request could work. 

![image](/uploads/2d64a2d25fd466e4dd1a5291f45918d0/image.png)

After making these changes I then began to write the ESP32 code so that it could actually make the HTTP requests. I learned that to make a request you essentially had to use a URL with all the necessary information to send.

![image](/uploads/d9bea21aa51b2f2fd23866bde9b78093/image.png)

The format the URL takes is that it first needs to have the IP address as well as the directory of where the php scripts are located on the web server. Then you set the values for all the variables the php scripts requires or else it would not work. Once I got the format figured out for the 3 php scripts I was able to finalize the ESP32 code and test if it worked. 

![image](/uploads/a004b6ae3836ffd5ac0c137cd26c6435/image.png)

The code shows the code that would be used to write an entry on the unknown table of our sql server via an HTTP request. I did not have many issues while testing I just made the mistake of not using the correct IP address when connecting the ESP32 and the local server. 

# 2023-04-24 Finalizing GUI 
Brennan and I met on this day to check out the GUI that Brennan worked on. He made a good looking GUI that allowed you to check out all the tables on our database but the only issue was that you had to type the name of the database and the table on the GUI inorder to have access to the entries as shown below. 

![image](/uploads/c34248f191a6379c13be4232c92fc0a0/image.png)

Another issue was that he could not delete entries of tables correctly. After we stopped working together I began to fix the issue with deleting entries on a specific table. The issue was that Brennan had reused the same variable names so the GUI would not behave accordingly. After getting this issue fixed I then began to improve the GUI by instead implementing a dropdown feature where instead of typing the database name you would choose a table from a dropdown.

![image](/uploads/be559633047ef340c4659c08d1700153/image.png)


After getting this feature to work I then began to add functionality to all the options the user would have depending on the SQL table they were looking at. For example for the client_list table I added the functionality to use the NFC reader on the raspberry to add users as shown below.

![image](/uploads/55692a161eea7a7abdef9d5e7aa7a2a2/image.png)

I also only made it so that whenever you were on a specific table the modification options you had access to were the ones that had functionality for that specific table so that our SQL GUI would not crash abruptly. 

This same day I tested the raspberry pi and our breadboard system and they all worked accordingly I was even able to accurately send the time whe I person scanned and log into our logs table as shown below:

![image](/uploads/2553f630700a963c5ba759f8cf70b95a/image.png)

# 2023-04-25 Final Changes and PCB work
On this day I made a few more quality of life changes to the GUI. I added an option for the logs table to display liable users. What I mean by this is that new window would pop out when you clicked the liable option on the screen and display all the users that had a counter that was greater than 1. Most of the day I was with Ege in the ECE lab trying to get our PCB to work. We tried many things but towards the end of the night we realized that our PCB was not going to work so we tried our best to find solutions because we had to present the following day. On this same day I used a carboard box to make a case for our breadboard system incase our pcb did not work. 

![image](/uploads/c091181ad6b80a980f6e2d48f22e8807/image.png)

# 2023-04-26 Our Final Demo
Before our demo I made sure our breadboard system was calibrated as well as functioning correctly so that during our final demo we would have no issues. We unfortunetly could not get our pcb to work so we had to demo with our breadboard system, but we did have everything else working accordingly. 
